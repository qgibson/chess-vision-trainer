
# Chess Vision Trainer

## Description

A fun chess based minigame where the goal is to click the correct square by its chess notation name.

Powered by Vite ⚡
## Features

	- Made with React, Typescript, Storybook, Unocss, and Vite⚡!
	- Correctly find as many chess squares as possible
	- Add your high score to the leaderboards

## How to install

Node is required and only (v16.17) has been tested

This project was bootstrapped using vite and share build commands

	- Clone the project
	- Run `npm install` in the root directory to install the dependencies
	- Run `npm run dev` to start developing

## How to use the project

Select new game to go to the game screen and start a game using the "Start Game" button.
The timer will start. You have until the timer is over to correctly pick as many sqaures
as you can.
## Credit

Shoutout to Nick Ram for giving me the idea for this project
